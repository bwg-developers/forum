# Demo Forum

데모 포럼 개발

## Git
```
git clone https://gitlab.com/inyeob2244/forum.git
git pull 
git checkout -b feature_inyeobkim
git add .
git commit -m "수정 사항"
git push origin feature_inyeobkim
```

## 실행 방법
### 리액트 앱 실행
```bash
cd forum/forum-demo/src/main/frontend 
npm run start
```
### 스프링부트 실행
```bash
cd forum-demo
gradlew build
gradlew bootRun
```

### 참고자료
https://dev.to/novu/building-a-forum-with-react-nodejs-6pe<br>
https://velog.io/@nyong_i/Jwt%EB%A1%9C-%EB%A1%9C%EA%B7%B8%EC%9D%B8-%EA%B5%AC%ED%98%84%ED%95%98%EA%B8%B0<br>
