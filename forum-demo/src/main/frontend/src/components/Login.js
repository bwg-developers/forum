import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";


import bwgGroupImage from '../static/img/bwg-thread-group.png';
import imageStyles from '../static/img/image.css';

const isLoginSuccessful = true;

const Login = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    
    const navigate = useNavigate();

    const handleSubmit = (e) => {

        e.preventDefault();
        console.log({ email, password });

        setEmail("");
        setPassword("");
 
        if (isLoginSuccessful) {
            console.log("login successful!");
            navigate("/dashboard");
        }

    };

    return (
        <main className='login'>
            <div className="loginHeader">
                {/* <h1 className='loginTitle'>BwG Thread Login</h1> */}
                <img id="loginImage" src={bwgGroupImage} style={imageStyles} alt="Login Page"></img> 
            </div>
            <form className='loginForm' onSubmit={handleSubmit}>
                <label htmlFor='email'>Email Address</label>
                <input
                    type='text'
                    name='email'
                    id='email'
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <label htmlFor='password'>Password</label>
                <input
                    type='password'
                    name='password'
                    id='password'
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <div className="loginBottom">
                    <button className='loginBtn'>SIGN IN</button>
                    <p>
                        Don't have an account? <Link to='/register'>Create one</Link>
                    </p>
                </div>
            </form>
        </main>
    );
};
export default Login;