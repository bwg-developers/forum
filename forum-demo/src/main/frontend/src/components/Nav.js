import React from "react";

import navLogo from '../static/img/bwg-thread-logo-white-transparent.png';
import imageStyles from '../static/img/image.css';

const Nav = () => {
    
    const signOut = () => {
        alert("User signed out!");
    };

    return (
        <nav className='navbar'>
            <img id="navbarLogoImage" src={navLogo} styles={imageStyles} alt="Navbar"></img>
            <div className='navbarRight'>
                <button onClick={signOut}>Sign out</button>
            </div>
        </nav>
    );
};

export default Nav;