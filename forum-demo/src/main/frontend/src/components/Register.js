import axios from 'axios';
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import SucessModal from './Modal/SucessModal';
import WarningModal from './Modal/WarningModal';

import groupImage from '../static/img/group-img.png';
import imageStyles from '../static/img/image.css';


const Register = () => {

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [showSuccessModal, setShowSuccessModal] = useState(false);
    const [showWarningModal, setShowWarningModal] = useState(false);

    const navigate = useNavigate();

    // function for checking input email
    const emailRegex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
    const isValidEmail = () => emailRegex.test(email);

    // function for checking input password
    const passwordMatch = () => password === passwordConfirm;

    const signUp = async() => {

        await axios.post("http://localhost:8080/api/register", {
            username: username,
            email: email,
            password: password,
        },
        {
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(response => {
            console.log(response);
            console.log("register successful!");
            navigate('/');
        })
        .catch(error => {
            console.log(error);
            alert("An error occured during registeration. Please contact the person in charge.");
        }) 

    }

    const handleSubmit = (e) => {

        e.preventDefault();

        if (isValidEmail() && passwordMatch()) {
            signUp();
        } else if (!isValidEmail() && passwordMatch()) {
            alert("The input email is not valid!");
            setEmail("");
        } else if (isValidEmail() && !passwordMatch()) {
            alert("Password does not match!");
            setPassword("");
            setPasswordConfirm("");
        } else {
            alert("Please check the input email or password!");
            setEmail("");
            setPassword("");
            setPasswordConfirm("");
        }
    };

    return (
        <main className='register'>
            <img id="registerImage" src={groupImage} styles={imageStyles} alt="Registeration Page" ></img>
            <h1 className='registerTitle'>Welcome to BwG Thread!</h1>
            <form className='registerForm' onSubmit={handleSubmit}>
                <label htmlFor='username'>Username</label>
                <input
                    type='text'
                    name='username'
                    id='username'
                    required
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <label htmlFor='email'>Email Address</label>
                    {email && (
                        <p
                            className='emailValidationMessage'
                            style={{ color: isValidEmail() ? "green" : "red" }}
                        >
                            {isValidEmail() ? "" : "Invalid email"}
                        </p>
                    )}
                </div>
                <input
                    type='text'
                    name='email'
                    id='email'
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <label htmlFor='password'>Password</label>
                <input
                    type='password'
                    name='password'
                    id='password'
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <label htmlFor='password'>Confirm Password</label>
                    {passwordConfirm && (
                        <p
                            className='passwordValidationMessage'
                            style={{ color: passwordMatch() ? "green" : "red" }}
                        >
                            {passwordMatch() ? "Password match" : "Password does not match"}
                        </p>
                    )}
                </div>
                <input
                    type='password'
                    name='passwordConfirm'
                    id='passwordConfirm'
                    required
                    value={passwordConfirm}
                    onChange={(e) => setPasswordConfirm(e.target.value)}
                />
 
                <div className='registerBottom'>
                    <button className='registerBtn' aria-disabled="true">REGISTER</button>
                    <p>
                        Have an account? <Link to='/'>Sign in</Link>
                    </p>
                </div>
            </form>
        </main>
    );
};

export default Register;