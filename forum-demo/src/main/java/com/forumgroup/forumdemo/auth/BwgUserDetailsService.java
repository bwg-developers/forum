package com.forumgroup.forumdemo.auth;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.forumgroup.forumdemo.model.BwgUser;
import com.forumgroup.forumdemo.repository.BwgUserRepository;
 
public class BwgUserDetailsService implements UserDetailsService {
 
    @Autowired
    private BwgUserRepository bwgUserRepository;
     
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<BwgUser> bwgUser = bwgUserRepository.findByEmail(email);

        if (!bwgUser.isPresent()) {
            throw new UsernameNotFoundException("Bwg user not found"); // spring security exception
        }

        return new BwgUserDetails(bwgUser.get());
    }
 
}