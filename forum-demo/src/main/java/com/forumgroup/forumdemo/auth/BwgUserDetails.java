package com.forumgroup.forumdemo.auth;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.forumgroup.forumdemo.model.BwgUser;
 
public class BwgUserDetails implements UserDetails {
 
    private BwgUser bwgUser;
     
    public BwgUserDetails(BwgUser bwgUser) {
        this.bwgUser = bwgUser;
    }
 
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
 
    @Override
    public String getPassword() {
        return bwgUser.getPassword();
    }
 
    @Override
    public String getUsername() {
        return bwgUser.getEmail();
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return true;
    }

 
}