package com.forumgroup.forumdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.forumgroup.forumdemo.model.BwgUser;
import com.forumgroup.forumdemo.repository.BwgUserRepositoryTest;

@Service
@Component
public class BwgUserFetchService {
    
    @Autowired
    private BwgUserRepositoryTest bwgUserRepositoryTest;

    public BwgUser getUserByUsername(String username) {
        return bwgUserRepositoryTest.findByUsername(username);
    }

}
