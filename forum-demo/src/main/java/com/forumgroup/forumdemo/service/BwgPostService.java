package com.forumgroup.forumdemo.service;

import org.springframework.stereotype.Service;

import com.forumgroup.forumdemo.dto.BwgPostRequestDto;
import com.forumgroup.forumdemo.enums.PostStatus;
import com.forumgroup.forumdemo.model.BwgPost;
import com.forumgroup.forumdemo.repository.BwgPostRepository;

@Service
public class BwgPostService {

    private BwgPostRepository bwgPostRepository;

    public BwgPostService(BwgPostRepository bwgPostRepository) {
        this.bwgPostRepository = bwgPostRepository;
    }


    public Long saveNewActivePost(BwgPostRequestDto postRequestDto) throws Exception {

        BwgPost newPost = postRequestDto.toEntity();
        newPost.setPostStatus(PostStatus.ACTIVE);

        BwgPost savedPost = this.bwgPostRepository.save(newPost);

        return savedPost.getId();
    }

    public Long saveNewTemporaryPost(BwgPostRequestDto postRequestDto) {

        BwgPost newPost = postRequestDto.toEntity();
        newPost.setPostStatus(PostStatus.TEMPORARY);

        BwgPost savedPost = this.bwgPostRepository.save(newPost);

        return savedPost.getId();
    }
}
