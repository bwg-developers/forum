package com.forumgroup.forumdemo.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.forumgroup.forumdemo.dto.RegisterationRequestDto;
import com.forumgroup.forumdemo.model.BwgUser;
import com.forumgroup.forumdemo.repository.BwgUserRepository;

@Service 
@Component
public class AuthService {

    private BwgUserRepository bwgUserRepository;

    public AuthService(BwgUserRepository bwgUserRepository) {
        this.bwgUserRepository = bwgUserRepository;
    }

    public String registerUser(RegisterationRequestDto request) throws Exception {

        String email = request.getEmail();

        if (bwgUserRepository.findByEmail(email).isPresent()) {
            throw new Exception("이미 등록된 이메일입니다.");
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        BwgUser bwgUser = request.toEntity();
        bwgUser.setPassword(passwordEncoder.encode(bwgUser.getPassword()));

        BwgUser registeredUser = bwgUserRepository.save(bwgUser);

        return registeredUser.getEmail();
    }


}
