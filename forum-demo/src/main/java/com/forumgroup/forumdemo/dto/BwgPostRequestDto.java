package com.forumgroup.forumdemo.dto;

import com.forumgroup.forumdemo.enums.PostStatus;
import com.forumgroup.forumdemo.model.BwgPost;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder 
@AllArgsConstructor
public class BwgPostRequestDto {

    private long id;
    private String title;
    private String content;
    private String author;
    private String category;
    private PostStatus postStatus;
    private int viewCount;
    private int likeCount;

    public BwgPost toEntity() {

        return BwgPost.builder()
                    .title(title)
                    .content(content)
                    .author(author)
                    .category(category)
                    .postStatus(postStatus)
                    .viewCount(viewCount)
                    .likeCount(likeCount)
                    .build();
    }
    


}
