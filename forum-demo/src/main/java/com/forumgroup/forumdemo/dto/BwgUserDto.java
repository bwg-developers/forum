package com.forumgroup.forumdemo.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BwgUserDto {
    
    private Long id;
    private String username;
    private String email;
    private String password;

}
