package com.forumgroup.forumdemo.dto;

import com.forumgroup.forumdemo.enums.Role;
import com.forumgroup.forumdemo.model.BwgUser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
@AllArgsConstructor
public class RegisterationRequestDto {

    private String username;
    private String email;
    private String password;
    private Role role;

    @Builder
    public BwgUser toEntity() {
        return BwgUser.builder()
                .username(username)
                .email(email)
                .password(password)
                .role(Role.USER)
                .build();
    }
}
