package com.forumgroup.forumdemo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.forumgroup.forumdemo.dto.RegisterationRequestDto;
import com.forumgroup.forumdemo.service.AuthService; 

@RestController
@RequestMapping("/api")
public class AuthController {

    private AuthService authService; 

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public String registerUser(@RequestBody RegisterationRequestDto registerRequest) throws Exception {

        System.out.println("AuthController > registerUser method invoked!!");

        authService.registerUser(registerRequest);
        
        return "success!";
    }

}
