package com.forumgroup.forumdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.forumgroup.forumdemo.model.BwgUser;
import com.forumgroup.forumdemo.service.BwgUserFetchService;


@RestController
@Component
public class BwgUserFetchController {

    @Autowired
    private BwgUserFetchService bwgUserFetchService; 

    @GetMapping("/api/users")
    public ResponseEntity<List<BwgUser>> getBwgUsers() {
        // findUser는 User 객체를 그대로 반환하고 있습니다. 이러한 경우의 문제는 클라이언트가 예상하는 HttpStatus를 설정해줄 수 없다는 것입니다. 
        // 예를 들어 어떤 객체의 생성 요청이라면 201 CREATED를 기대할 것이지만 객체를 그대로 반환하면 HttpStatus를 설정해줄 수 없습니다. 
        // 그래서 객체를 상황에 맞는 ResponseEntity로 감싸서 반환해주어야 합니다.
        return null;
    }

}
