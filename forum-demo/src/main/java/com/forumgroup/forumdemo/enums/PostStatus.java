package com.forumgroup.forumdemo.enums;

public enum PostStatus {

    ACTIVE,
    DEACTIVATED,
    TEMPORARY,
    
}
