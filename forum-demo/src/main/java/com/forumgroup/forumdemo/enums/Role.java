package com.forumgroup.forumdemo.enums;

public enum Role {

    USER,
    MANAGER,
    ADMIN;
    
}
