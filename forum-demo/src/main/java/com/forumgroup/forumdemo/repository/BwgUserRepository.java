package com.forumgroup.forumdemo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.forumgroup.forumdemo.model.BwgUser;

// JpaRepository is interface is a subtype of JpaRepository which defines common persistence operations (including CRUD) and the implementation will be generated at runtime by Spring Data JPA.
public interface BwgUserRepository extends JpaRepository<BwgUser, Long>{

    // @Query("SELECT u FROM bwg_users u WHERE u.email = ?1")
    public Optional<BwgUser> findByEmail(String email);
    public boolean existsByEmail(String email);
    
}
