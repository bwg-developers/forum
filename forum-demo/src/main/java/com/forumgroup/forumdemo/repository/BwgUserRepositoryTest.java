package com.forumgroup.forumdemo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.forumgroup.forumdemo.dto.BwgUserDto;
import com.forumgroup.forumdemo.enums.Role;
import com.forumgroup.forumdemo.model.BwgUser;

@Repository
public class BwgUserRepositoryTest {
    
    private static List<BwgUser> userList = new ArrayList<>();
    
    public BwgUserRepositoryTest() {

  
    }

    public static List<BwgUser> getUsers() {
        return userList;
    }

    public BwgUser findByUsername(String username) {
        for (BwgUser user : userList) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    public String saveUser(BwgUserDto userDto) {
 
        // BwgUser newUser = new BwgUser(); 
        // newUser.setUsername(userDto.getUsername());
        // newUser.setEmail(userDto.getEmail());
        // newUser.setPassword(userDto.getPassword());

        // userList.add(newUser);

        // System.out.println("new user registered successful.!");
        // System.out.println("username: " + newUser.getUsername());
        // System.out.println("email: " + newUser.getEmail());

        // System.out.println("total user list size = " + userList.size());

        // return newUser.getEmail();
        return null;
    } 

}
