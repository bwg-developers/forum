package com.forumgroup.forumdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.forumgroup.forumdemo.model.BwgPost;

public interface BwgPostRepository extends JpaRepository<BwgPost, Long>{
    

    
}
