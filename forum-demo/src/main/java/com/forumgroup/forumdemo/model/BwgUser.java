package com.forumgroup.forumdemo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forumgroup.forumdemo.enums.Role;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="bwg_users")
@Getter
@Setter
@Builder
public class BwgUser extends BaseTimeEntity{ 
    
    @Id
    @JsonProperty(value="Id")
    @Column(name="id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @JsonProperty(value="Username")
    @Column(name="username", nullable=false, unique = true, length=45)
    String username;

    @JsonProperty(value="Email")
    @Column(name = "email", nullable=false, unique=true, length=45)
    String email;

    @JsonProperty(value = "Password")
    @Column(name = "password")
    String password;

    @JsonProperty
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Builder
    public BwgUser(Long id, String username, String email, String password, Role role) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
    }

}
