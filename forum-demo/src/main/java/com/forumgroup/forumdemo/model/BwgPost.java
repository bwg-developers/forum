package com.forumgroup.forumdemo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forumgroup.forumdemo.enums.PostStatus;

import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BwgPost {

    @Id
    @JsonProperty(value="Id")
    @Column(name="id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty(value="Title")
    @Column(name="title", nullable=false)
    private String title;

    @JsonProperty(value="Content")
    @Column(name = "content", nullable=false)
    private String content;
    
    @JsonProperty(value="Author")
    @Column(name = "author", nullable = false)
    private String author;

    @JsonProperty(value = "Category")
    @Column(name = "category", nullable = false)
    private String category;

    @JsonProperty(value = "PostStatus")
    @Column(name = "post_status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private PostStatus postStatus;

    @JsonProperty(value = "ViewCount")
    @Column(name = "view_count")
    private int viewCount;

    @JsonProperty(value = "LikeCount")
    @Column(name = "like_count")
    private int likeCount;

    public boolean isActive() {
        return PostStatus.ACTIVE == this.postStatus;
    }

}
