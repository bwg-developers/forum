package com.forumgroup.forumdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootTest
@EnableJpaAuditing // BaseTimeEntity를 만들었다면 꼭 Application에 @EnableJpaAuditing을 추가해줘야 한다.
class ForumDemoApplicationTests {

	@Test
	void contextLoads() {
	}

}
